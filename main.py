#!/usr/bin/env python3
"""Script to convert VATSIM airspaces to the OpenAir format."""
import json
import logging
import re
from io import TextIOWrapper
from typing import Dict, List, Tuple

import colorlog
from shapely.geometry import Polygon
from shapely.geometry.multipolygon import MultiPolygon
from shapely.ops import unary_union

logger = logging.getLogger(__name__)


def main():
    """Main."""
    setup_logging()
    with open("FIRBoundaries.dat") as f:
        fir_boundaries = read_boundaries(f)
    with open("VATSpy.dat") as f:
        (firs_to_icao, uirs_to_icao) = read_mappings(f)
    uir_boundaries = create_uir_boundaries(uirs_to_icao, fir_boundaries)
    with open("vatsim_airspaces.txt", "w") as f:
        export_fir_boundaries(firs_to_icao, fir_boundaries, f)
        export_uir_boundaries(uir_boundaries, f)


def setup_logging():
    """Initialize the logging functionality."""
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    log_format = "%(asctime)s - %(name)s - %(log_color)s%(levelname)s%(reset)s - %(message)s"
    formatter = colorlog.ColoredFormatter(log_format)
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    root_logger.addHandler(handler)


def read_boundaries(f: TextIOWrapper) -> Dict[str, List[Tuple[float, float]]]:
    """Read all FIR metadata and their boundaries."""
    boundaries = {}
    line = f.readline()
    while line:
        (
            icao,
            _is_oceanic,
            _is_extension,
            point_count,
            _min_lat,
            _min_lon,
            _max_lat,
            _max_lon,
            _center_lat,
            _center_lon
        ) = tuple(line.split("|"))
        point_count = int(point_count)
        if point_count < 3:
            # Skip this airspace
            logger.warning("boundary %s has less than 3 points (%d points)", icao, point_count)
            for _ in range(point_count + 1):
                line = f.readline()
            continue
        boundary = []
        for _ in range(point_count):
            (lat, lon) = tuple(f.readline().split("|"))
            lat = float(lat)
            lon = float(lon)
            boundary.append((lat, lon))
        if icao in boundaries:
            # ICAO already exists, merge the two boundaries
            poly_1 = Polygon(boundary).buffer(0)
            poly_2 = Polygon(boundaries[icao]).buffer(0)
            boundary = unary_union([poly_1, poly_2])
        boundaries[icao] = boundary
        logger.info("boundary %s read with %d points", icao, point_count)
        line = f.readline()
    # Store created `Polygon` and `MultiPolygon` objects as coordinates
    cleaned_boundaries = boundaries.copy()
    for icao, boundary in boundaries.items():
        if isinstance(boundary, MultiPolygon):
            logger.error("couldn't merge FIR boundaries with same ICAO %s", icao)
            cleaned_boundaries.pop(icao)
        elif isinstance(boundary, Polygon):
            cleaned_boundaries[icao] = boundary.exterior.coords
    return cleaned_boundaries


def read_mappings(f: TextIOWrapper) -> Tuple[Dict[str, str], Dict[str, List[str]]]:
    """Read all `prefix -> ICAO(s)` mappings."""
    firs_mapping = {}
    uirs_mapping = {}
    data = ""
    for line in f:
        # Ignore comments
        if not line.startswith(";"):
            data += line
    sections = re.split(r'\[\w+\]', data)
    sections = [text.strip() for text in sections[1:]]
    (_countries, _airports, firs, uirs, _idl) = tuple(sections)
    firs_mapping = read_fir_mappings(firs)
    uirs_mapping = read_uir_mappings(uirs)
    return (firs_mapping, uirs_mapping)


def read_fir_mappings(fir_dat_section: str) -> dict:
    """Read all `prefix/VATSIM position name -> FIR ICAO`."""
    firs_mapping = {}
    for fir in fir_dat_section.split("\n"):
        (icao, _name, prefix_position, fir_name) = tuple(fir.split("|"))
        fir_name = fir_name.replace("_", "-")  # Fix the data ...
        if prefix_position and fir_name:
            # Good data ...
            if fir_name.startswith(prefix_position) and prefix_position != fir_name:
                logger.warning(
                    "FIR name %s starts with prefix %s, using FIR name as prefix",
                    fir_name,
                    prefix_position
                )
                prefix_position = fir_name
            firs_mapping[prefix_position] = fir_name
        elif fir_name:
            # No prefix
            firs_mapping[fir_name] = fir_name
        elif prefix_position:
            # Prefix but no boundary identifier ...
            # Let's use the ICAO in this case
            firs_mapping[prefix_position] = icao
    with open("custom_mappings.json") as f:
        data = json.load(f)
        firs_mapping.update(data)
    return firs_mapping


def read_uir_mappings(uir_dat_section: str) -> dict:
    """Read all `prefix/VATSIM position name -> FIR ICAOs that comprise the UIR`."""
    uirs_mapping = {}
    for uir_def in uir_dat_section.split("\n"):
        (icao, _name, area) = uir_def.split("|")
        area = area.split(",")
        uirs_mapping[icao] = area
    return uirs_mapping


def create_uir_boundaries(
        uir_mappings: Dict[str, List[str]],
        boundaries: Dict[str, List[Tuple[float, float]]]) -> Dict[str, List[Tuple[float, float]]]:
    """Merge all FIR regions of an UIR into a single UIR airspace."""
    uir_boundaries = {}
    for uir_icao, areas in uir_mappings.items():
        polys = []
        for fir_icao in areas:
            if fir_icao not in boundaries:
                logger.error("FIR %s not found for UIR %s", fir_icao, uir_icao)
                continue
            polys.append(Polygon(boundaries[fir_icao]).buffer(0))
        uir_boundary = unary_union(polys)
        if isinstance(uir_boundary, MultiPolygon):
            logger.error(
                "unary_union did produce MultiPolygon for UIR %s", uir_icao)
        elif isinstance(uir_boundary, Polygon):
            # pylint: disable=no-member
            uir_boundaries[uir_icao] = uir_boundary.exterior.coords
        else:
            logger.error("UIR boundary is of type %s", type(uir_boundary))
    return uir_boundaries


def export_fir_boundaries(
        firs_to_icao: Dict[str, str],
        boundaries: Dict[str, List[Tuple[float, float]]],
        f: TextIOWrapper):
    """Export all FIR boundaries to the specified OpenAir file."""
    for prefix, fir_icao in firs_to_icao.items():
        if fir_icao not in boundaries:
            logger.error("FIR %s is not mapped, ignoring", fir_icao)
            continue
        boundary = boundaries[fir_icao]
        f.write(boundary_to_openair(prefix + "_CTR", boundary))
        f.write(boundary_to_openair(prefix + "_FSS", boundary))

    # Export boundaries that are not referenced in the map
    unreferenced_boundaries = set(boundaries.keys()).difference(
        firs_to_icao.values())
    for boundary_icao in unreferenced_boundaries:
        logger.warning(
            "FIR boundary %s is never referenced, exporting with ICAO name",
            boundary_icao
        )
        name = boundary_icao.replace("-", "_")
        boundary = boundaries[boundary_icao]
        f.write(boundary_to_openair(name + "_CTR", boundary))
        f.write(boundary_to_openair(name + "_FSS", boundary))

    # Export with original FIR name because some mappings are outdated?
    alternate_prefixes = set(boundaries.keys()) \
        .difference(firs_to_icao.keys()) \
        .difference(unreferenced_boundaries)
    for fir_icao in alternate_prefixes:
        name = fir_icao.replace("-", "_")
        boundary = boundaries[fir_icao]
        f.write(boundary_to_openair(name + "_CTR", boundary))
        f.write(boundary_to_openair(name + "_FSS", boundary))


def export_uir_boundaries(boundaries: Dict[str, List[Tuple[float, float]]], f: TextIOWrapper):
    """Export all UIR boundaries to the specified OpenAir file."""
    for icao, boundary in boundaries.items():
        name = icao + "_FSS"
        f.write(boundary_to_openair(name, boundary))


def boundary_to_openair(
        name: str,
        boundary: List[Tuple[float, float]],
        airspace_type="CTR") -> str:
    """Converts a boundary to an OpenAir string."""
    open_air = f"AC {airspace_type}\r\nAN {name}\r\n"
    for (lat_dec, lon_dec) in boundary:
        (lat_d, lat_m, lat_s) = dd2dms(lat_dec)
        (lon_d, lon_m, lon_s) = dd2dms(lon_dec)
        n_s = "N" if lat_dec >= 0 else "S"
        e_w = "E" if lon_dec >= 0 else "W"
        open_air += \
            f"DP {lat_d:>02d}:{lat_m:>02d}:{lat_s:>02d} {n_s}" \
            + f" {lon_d:>02d}:{lon_m:>02d}:{lon_s:>02d} {e_w}\r\n"
    return open_air


def dd2dms(coordinate: float):
    """Convert the coordinate from DD to DMS."""
    # pylint: disable=invalid-name
    d = int(coordinate)
    m = int((coordinate - d) * 60)
    s = int((coordinate - d - m / 60) * 3600)
    return (abs(d), abs(m), abs(s))


if __name__ == "__main__":
    main()
